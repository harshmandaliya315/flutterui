import 'package:flutter/material.dart';

class Wanderly extends StatelessWidget {
  const Wanderly({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/beach-house-5.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Wanderly',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 44,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Roboto-Regular'),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Your utlimate companion for seamless',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontFamily: 'Roboto-Light')),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Travel Experiences',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontFamily: 'Roboto-Light'),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 400,
                  ),
                  TextButton(
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all(EdgeInsets.zero),
                      // Remove internal padding
                      tapTargetSize: MaterialTapTargetSize
                          .shrinkWrap, // Remove minimum size constraints
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/house');
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50.0),
                          color: Colors.lightBlue),
                      padding: EdgeInsets.symmetric(vertical: 17, horizontal: 70),
                      child: Text(
                        'Sign in with Phone Number',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontFamily: 'Roboto-Light'),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  TextButton(
                    style: ButtonStyle(
                      padding: MaterialStateProperty.all(EdgeInsets.zero),
                      // Remove internal padding
                      // tapTargetSize: MaterialTapTargetSize
                      //     .shrinkWrap, // Remove minimum size constraints
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/house');
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50.0),
                          color: Colors.white),
                      padding: EdgeInsets.symmetric(vertical: 16, horizontal: 50),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.apple,
                            color: Colors.black,
                            size: 27,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'Sign in with Apple',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18,
                                fontFamily: 'Roboto-Light'),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Don't have an account? Sign up",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                              fontFamily: 'Roboto-Light'
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "By creating an account or signing in, you agree to",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                            fontFamily: 'Roboto-Light'
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "out Terms of Service and Privacy Policy",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                            fontFamily: 'Roboto-Light'
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
