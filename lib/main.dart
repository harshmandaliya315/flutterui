import 'package:assignment_screens/crypto_portfolio.dart';
import 'package:assignment_screens/forest_house.dart';
import 'package:assignment_screens/investment.dart';
import 'package:assignment_screens/wanderly.dart';
import 'package:flutter/material.dart';
void main()=> runApp(
  MaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: '/wander',
    routes: {
      '/wander' : (context) => Wanderly(),
      '/house' : (context) => forest_house(),
      '/invest' : (context) => Investment(),
      '/portfolio' : (context) => Crypto_portfolio(),
    },
  ),
);