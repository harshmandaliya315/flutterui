import 'package:flutter/material.dart';

class Crypto_portfolio extends StatefulWidget {
  const Crypto_portfolio({Key? key}) : super(key: key);

  @override
  State<Crypto_portfolio> createState() => _Crypto_portfolioState();
}

final List<String> coins = [
  'Bitcoin',
  'Ethereum',
  'Cardano',
  'Solana',
  'Polkadot',
  'Avalanche',
  'Binance Coin',
  'Terra',
];
final List<String> coinData = [
  '0.07316 BTC',
  '2.5348 ETH',
  '1.3257 ADA',
  '150.123 SOL',
  '30.467 DOT',
  '40.210 AVAX',
  '15.678 BNB',
  '5.892 LUNA',
];
final List<String> coinLogos = [
  'assets/images/bitcoin.png',
  'assets/images/ethereum.png',
  'assets/images/cardano.png',
  'assets/images/solana.png',
  'assets/images/polkadot.png',
  'assets/images/avalanche.png',
  'assets/images/binance.png',
  'assets/images/terra.png',
];
final List<dynamic> indicator = [
  Icon(
    Icons.trending_down,
    size: 14,
    color: Colors.red,
  ),
  Icon(
    Icons.trending_up,
    size: 14,
    color: Colors.lightGreen,
  ),
  Icon(
    Icons.trending_down,
    size: 14,
    color: Colors.red,
  ),
  Icon(
    Icons.trending_up,
    size: 14,
    color: Colors.lightGreen,
  ),
  Icon(
    Icons.trending_up,
    size: 14,
    color: Colors.lightGreen,
  ),
  Icon(
    Icons.trending_up,
    size: 14,
    color: Colors.lightGreen,
  ),
  Icon(
    Icons.trending_up,
    size: 14,
    color: Colors.lightGreen,
  )
];
final List<dynamic> gains = [
  Text(
    '-2.04%',
    style: TextStyle(color: Colors.red, fontSize: 14),
  ),
  Text(
    '+4.34%',
    style: TextStyle(color: Colors.lightGreen, fontSize: 14),
  ),
  Text(
    '-4.92%',
    style: TextStyle(color: Colors.red, fontSize: 14),
  ),
  Text(
    '+5.84%',
    style: TextStyle(color: Colors.lightGreen, fontSize: 14),
  ),
  Text(
    '+9.36%',
    style: TextStyle(color: Colors.lightGreen, fontSize: 14),
  ),
  Text(
    '+0.13%',
    style: TextStyle(color: Colors.lightGreen, fontSize: 14),
  ),
  Text(
    '+6.69%',
    style: TextStyle(color: Colors.lightGreen, fontSize: 14),
  ),
];
final List<String> holdings = [
  '\$2139.68', // Bitcoin
  '\$2534.8', // Ethereum
  '\$1.23', // Cardano
  '\$149.5', // Solana
  '\$28.7', // Polkadot
  '\$56.2', // Avalanche
  '\$324.9', // Binance Coin
  '\$18.3', // Terra
];

class _Crypto_portfolioState extends State<Crypto_portfolio> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            radius: 20,
                            backgroundImage:
                            AssetImage('assets/images/person3f.jpg'),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'Owen',
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 19),
                          ),
                          Icon(
                            Icons.keyboard_arrow_right,
                            color: Colors.grey,
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.notifications_none,
                            size: 27,
                          )
                        ],
                      )
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.all(Radius.circular(14))),
                    height: 45,
                    width: double.infinity,
                    child: Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Icon(
                          Icons.search_rounded,
                          color: Colors.grey,
                          size: 30,
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        Text(
                          'Search',
                          style: TextStyle(color: Colors.grey, fontSize: 20),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      RichText(
                        text: TextSpan(
                          style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                          children: [
                            TextSpan(
                              text: 'Total balance in',
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                            TextSpan(
                              text: ' USD',
                              style: TextStyle(
                                color: Colors.blue,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Text(
                        '\$34,151.37',
                        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.arrow_upward,
                        size: 15,
                        color: Colors.lightGreen,
                      ),
                      Text(
                        '+\$562.15',
                        style: TextStyle(color: Colors.lightGreen),
                      ),
                      Text(
                        ' total',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        image: DecorationImage(
                            image: AssetImage('assets/images/crypto1.jpg'),
                            fit: BoxFit.cover)),
                    width: double.infinity,
                    height: 100,
                    padding: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Crypto Exchange',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700),
                            ),
                            CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 9,
                              child: Icon(
                                Icons.close_rounded,
                                color: Colors.grey,
                                size: 15,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              'Trusted by milloins, Low fees,',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          children: [
                            Text(
                              'Fastest trades, USD , EUR and GBP',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Coins',
                        style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                      ),
                      Icon(
                        Icons.filter_list,
                        color: Colors.grey,
                        size: 30,
                      )
                    ],
                  ),
                  Expanded(
                    flex: 8,
                    child: ListView.builder(
                      itemCount: 7,
                      itemBuilder: (context, index) {
                        return Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Text(
                                            coins[index],
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 17,
                                                color: Colors.grey),
                                          ),
                                          SizedBox(
                                            width: 7,
                                          ),
                                          indicator[index],
                                          gains[index],
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            coinData[index],
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          SizedBox(
                                            width: 8,
                                          ),
                                          Text(
                                            holdings[index],
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12),
                                          )
                                        ],
                                      )
                                    ],
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  CircleAvatar(
                                    foregroundImage: AssetImage(coinLogos[index]),
                                    radius: 20,
                                    backgroundColor: Colors.transparent,
                                  ),
                                ],
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 5,
                        blurRadius: 2,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  border: Border(
                    top: BorderSide(color: Colors.white)
                  )

                ),
                padding: EdgeInsets.symmetric(horizontal: 20),
                height: 80,
                width: double.infinity,

                child:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.home_outlined,size: 35,color: Colors.black,),
                        Text('Home',style: TextStyle(fontSize: 12),)
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.analytics_outlined,size: 35,color: Colors.black26,),
                        Text('Market',style: TextStyle(fontSize: 12,color: Colors.black26),)
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.developer_board,size: 35,color: Colors.black26,),
                        Text('Actions',style: TextStyle(fontSize: 12,color: Colors.black26),)
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(Icons.chat_bubble_outline,size: 35,color: Colors.black26,),
                        Text('chat',style: TextStyle(fontSize: 12,color: Colors.black26),)
                      ],
                    ),

                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
