import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

class Investment extends StatefulWidget {
  const Investment({Key? key}) : super(key: key);

  @override
  State<Investment> createState() => _InvestmentState();
}

class PersonInfo {
  final String imagePath;
  final String name;
  final String message;
  final IconData iconData;

  PersonInfo({
    required this.imagePath,
    required this.name,
    required this.message,
    required this.iconData,
  });
}
List<PersonInfo> persons = [
  PersonInfo(
    imagePath: 'assets/images/person1.jpg',
    name: 'aleis',
    message: 'today',
    iconData: Icons.home,
  ),
  PersonInfo(
    imagePath: 'assets/images/person2.jpg',
    name: 'max',
    message: 'today',
    iconData: Icons.lock,
  ),
  PersonInfo(
    imagePath: 'assets/images/person3f.jpg',
    name: 'lisa',
    message: 'today',
    iconData: Icons.crop_landscape,
  ),
];

class _InvestmentState extends State<Investment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 40),
        color: Color.fromARGB(255, 237, 228, 228),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          // Align children to the start
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CircleAvatar(
                  child: Icon(Icons.add_circle_outline, color: Colors.white),
                  backgroundColor: Colors.black,
                ),
                Icon(
                  Icons.account_circle,
                  size: 40,
                )
              ],
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  height: 225,
                  width: 158,
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // Align children to the start
                    children: [
                      Row(
                        children: [
                          CircularPercentIndicator(
                            radius: 40.0,
                            lineWidth: 8.0,
                            percent: 0.58,
                            center: Text('58%'),
                            progressColor: Colors.deepPurpleAccent,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        'DIS Invest',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                            fontSize: 15),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        '\$6.000',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 30),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Virtual Assistant',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                            fontSize: 15),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  height: 225,
                  width: 158,
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // Align children to the start
                    children: [
                      Row(
                        children: [
                          CircularPercentIndicator(
                            radius: 40.0,
                            lineWidth: 8.0,
                            percent: 0.23,
                            center: Text('23%'),
                            progressColor: Colors.orange,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        'AAPL Tech',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                            fontSize: 15),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        '\$4.350',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 30),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        '1 month left',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.grey,
                            fontSize: 15),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 40,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Latest',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Container(
                  width: 260, // Adjust the width as needed
                  height: 2, // Set the height of the line
                  color: Colors.grey, // Set the color of the line
                ),
              ],
            ),
            Expanded(
              flex: 4,
              child: ListView.builder(
                itemCount: 3, // Set the number of items
                itemBuilder: (context, index) {
                  return Container(
                    color: Color.fromARGB(255, 237, 228, 228),
                    padding: EdgeInsets.only(bottom: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            CircleAvatar(
                              foregroundImage: AssetImage(assignImage(index)),
                              radius: 30,
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      assignName(index),
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      assignMessage(index),
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              child: Icon(
                                assignIcon(index),
                                color: Color.fromARGB(255, 237, 228, 228),
                              ),
                              color: Colors.black,
                              height: 40,
                              width: 40,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Icon(
                              Icons.arrow_drop_down,
                              size: 40,
                            )
                          ],
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
            Expanded(
                flex: 1,
                child: TextButton(
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all(EdgeInsets.zero),
                    // Remove internal padding
                    tapTargetSize: MaterialTapTargetSize
                        .shrinkWrap, // Remove minimum size constraints
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/portfolio');
                  },
                  child: Container(
                    decoration: BoxDecoration(color: Colors.black),
                    padding: EdgeInsets.symmetric(vertical: 16, horizontal: 50),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Continue',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}

dynamic assignName(int index) {
  if (index >= 0 && index < persons.length) {
    return persons[index].name;
  }
  return '';
}
dynamic assignImage(int index) {

  if (index >= 0 && index < persons.length) {
    return persons[index].imagePath;
  }
  return '';
}
dynamic assignMessage(int index) {
  if (index >= 0 && index < persons.length) {
    return persons[index].message;
  }
  return '';
}
dynamic assignIcon(int index) {
  if (index >= 0 && index < persons.length) {
    return persons[index].iconData;
  }
  return Icons.wifi;
}

